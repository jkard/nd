/** A class for creating Student objects. */
public class StudentFactory {
  /**
   * Creates a new student object based on their degree.
   *
   * @param input - A string with the values for the object properties. The values should be *
   *     separated with ' ' symbols.
   * @return A student object created based on their degree.
   */
  public Student GetStudent(String input) {
    String[] inputArray = ProcessInput(input);

    String firstName = inputArray[0];
    String lastName = inputArray[1];
    String degree = inputArray[2];
    String field = inputArray[3];
    double avgGrades = Double.parseDouble(inputArray[4]);

    Student student = null;
    switch (degree) {
      case "bakalaurantas":
        student = new BacStudent(firstName, lastName, avgGrades);
        break;
      case "magistrantas":
        student = new MagStudent(firstName, lastName, field);
        break;
      case "doktorantas":
        student = new DocStudent(firstName, lastName);
        break;
    }
    return student;
  }

  /**
   * Splits the input string into smaller strings.
   *
   * @param originalInput - The original input string.
   * @return The original input string split into multiple smaller strings for each property.
   */
  private String[] ProcessInput(String originalInput) {
    return originalInput.split(" ");
  }
}
