/** A test class for the Student and StudentDegree classes. */
public class Test {
  public static void main(String[] args) {
    FactoryTest();
    RegularTest();
  }

  public static void FactoryTest() {
    System.out.println("\nInitiating the factory class test...");
    StudentFactory factory = new StudentFactory();

    Student a1 = factory.GetStudent("Jonas Jonaitis magistrantas filologija 10");
    System.out.println(a1.ToString());

    Student a2 = factory.GetStudent("Petras Petraitis doktorantas filosofija 4");
    System.out.println(a2.ToString());

    System.out.println("Factory class test complete!\n\n");
  }

  public static void RegularTest() {
    System.out.println("Initiating the regular test...");

    StudentDegree a1 = new StudentDegree("Jonas Jonaitis magistrantas filologija 10");
    System.out.println(a1.ToString());

    StudentDegree a2 = new StudentDegree("Petras Petraitis doktorantas filosofija 4");
    System.out.println(a2.ToString());

    System.out.println("Regular test complete!");
  }
}
