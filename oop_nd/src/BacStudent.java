/** A class describing a student studying a bachelor's degree. */
public class BacStudent extends Student {
  /**
   * @param firstName - First name.
   * @param lastName - Last name.
   * @param avgGrades - Average grades.
   */
  public BacStudent(String firstName, String lastName, double avgGrades) {
    FirstName = firstName;
    LastName = lastName;
    AvgGrades = avgGrades;
    Title = "";
    Scholarship = GetScholarship();
  }

  /** @return Scholarship amount. */
  public int GetScholarship() {
    if (AvgGrades > 8) {
      return 100;
    } else {
      return 0;
    }
  }
}
