/**
 * A class describing a student of a certain degree. The class inherits from the Student abstract
 * class.
 */
public class StudentDegree extends Student {
  /**
   * @param input - A string with the values for the object properties. The values should be
   *     separated with ' ' symbols.
   */
  public StudentDegree(String input) {
    String[] inputArray = ProcessInput(input);

    FirstName = inputArray[0];
    LastName = inputArray[1];
    AvgGrades = Double.parseDouble(inputArray[4]);
    Field = inputArray[3];
    Degree = inputArray[2];
    Title = ProcessTitle(Degree);
    Scholarship = ProcessScholarship(Degree);
  }

  /**
   * Splits the input string into smaller strings.
   *
   * @param originalInput - The original input string.
   * @return The original input string split into multiple smaller strings for each property.
   */
  private String[] ProcessInput(String originalInput) {
    return originalInput.split(" ");
  }

  /**
   * Finds out if the student's average grades are more than 8.
   *
   * @param averageGrades - The student's avg. grades.
   * @return True if the grades are higher than 8, otherwise - false.
   */
  private boolean ProcessGrades(double averageGrades) {
    return averageGrades > 8;
  }

  /**
   * Finds the academic title for the student.
   *
   * @param academicTitle - The student's academic title.
   * @return "Dr. " if the student is a doctoral student, otherwise - false.
   */
  private String ProcessTitle(String academicTitle) {
    if (academicTitle.equals("doktorantas")) {
      return "Dr. ";
    }
    return "";
  }

  /**
   * Finds out if the student is studying in a scientific study field.
   *
   * @param input - The student's study field.
   * @return True if the student is studying in scientific studies, otherwise - false.
   */
  private boolean ProcessStudies(String input) {
    return input.equals("matematika")
        || input.equals("informatika")
        || input.equals("fizika")
        || input.equals("chemija");
  }

  /**
   * Finds the scholarship amount based on the student's academic title.
   *
   * @param input - The student's academic title.
   * @return The student's scholarship amount in euros.
   */
  private int ProcessScholarship(String input) {
    int amount = 0;
    switch (input) {
      case "bakalaurantas":
        if (ProcessGrades(AvgGrades)) {
          amount = 100;
        }
        break;
      case "magistrantas":
        if (ProcessStudies(Field)) {
          amount = 200;
        }
        break;
      case "doktorantas":
        amount = 800;
        break;
    }
    return amount;
  }
}
