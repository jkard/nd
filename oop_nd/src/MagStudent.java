/** A class describing a student studying a magistrate degree. */
public class MagStudent extends Student {
  /**
   * @param firstName - First name.
   * @param lastName - Last name.
   * @param field - Field of studies.
   */
  public MagStudent(String firstName, String lastName, String field) {
    FirstName = firstName;
    LastName = lastName;
    Field = field;
    Title = "";
    Scholarship = GetScholarship();
  }

  /** @return Scholarship amount. */
  public int GetScholarship() {
    if (Field.equals("matematika")
        || Field.equals("informatika")
        || Field.equals("fizika")
        || Field.equals("chemija")) {
      return 200;
    } else {
      return 0;
    }
  }
}
