/** A abstract class describing a student. */
public abstract class Student {
  public String FirstName; // First name.
  public String LastName;  // Last name.
  public String Degree;    // Scientific degree.
  public String Field;     // Scientific field.
  public double AvgGrades; // Average grades.
  public String Title;     // Academic title - "Dr." for doctorate student, "" for others.
  public int Scholarship;  // Scholarship amount in euros.

  /**
   * A method for printing the properties of the current object.
   *
   * @return A string with the student's academic title, the first and last names, and the
   *     scholarship in euros.
   */
  public String ToString() {
    return Title + FirstName + " " + LastName + " gaus " + Scholarship + " eurų stipendiją";
  }
}
