/** A class describing a student studying a doctoral degree. */
public class DocStudent extends Student {
  /**
   * @param firstName - First name.
   * @param lastName - Last Name.
   */
  public DocStudent(String firstName, String lastName) {
    FirstName = firstName;
    LastName = lastName;
    Title = "Dr. ";
    Scholarship = 800;
  }
}
